import React, { FunctionComponent } from "react";

interface Props {
  image: string;
}

const Item: FunctionComponent<Props> = ({ image }) => {
  return (
    <>
      <div className="item-wrapper bg-[#f9f9ff] overflow-hidden duration-500">
        <div className="overflow-hidden">
          <img className="image duration-500" src={image} alt="" />
        </div>
        <div className="info p-[30px]">
          <div className="flex justify-between items-center mb-[20px] font-bold text-[25px] ">
            <h1 className="text-[#131230]">Name</h1>
            <p className="text-[#FFB606] duration-500">10$</p>
          </div>
          <p className="text-[#777]">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo ad
            consequuntur, cupiditate iste quae distinctio dolor doloribus libero
          </p>
        </div>
      </div>

      <style>
        {`
            
            .item-wrapper:hover{
                background-color:#FFB606;
                color:#131230;
            }
            .item-wrapper:hover *{
                color:#131230;

            }
            .item-wrapper:hover img{
                transform: scale(1.2);
            }
            `}
      </style>
    </>
  );
};

export default Item;

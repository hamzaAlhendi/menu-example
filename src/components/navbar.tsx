import React from "react";
import logo from "../assets/logo.png";
import { NavLink } from "react-router-dom";

const Navbar = () => {
  return (
    <>
      <div className="header">
        <NavLink to="">
          <img src={logo} className="logo" />
        </NavLink>
        <ul className="main-nav">
          <li>
            <NavLink to="">Home</NavLink>
          </li>
          <li>
            <NavLink to="/email">Email</NavLink>
          </li>
        </ul>
      </div>

      <style>
        {`

         
        .header {
            display: flex;
            flex-flow: row wrap;
            justify-content: space-between;
            align-items: center;
            padding-left: 15px;
            padding-right: 15px;
            margin-right: auto;
            margin-left: auto;
            margin-top:20px;
            position:absolute;
            z-index:1;
            left:50%;
            transform:translateX(-50%);
            background-color:white;
            border-radius:10px;
            width:80%;
          }

          @media (min-width: 768px) {
            .header {
              width: 750px;
            }
          }
          
          @media (min-width: 992px) {
            .header {
              width: 970px;
            }
          }

          @media (min-width: 1200px) {
            .header {
              width: 1170px;
            }
          }
          
          
           
           .logo {
            color: #ffb606;
            font-size: 26px;
            font-weight: bold;
            height: 72px;
            display: flex;
            justify-content: center;
            align-items: center;
          }
          
          @media (max-width: 767px) {
             .logo {
              width: 100%;
            }
          }
          
           .main-nav {
            display: flex;
          }
          
          
          
           .main-nav > li > a {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 72px;
            position: relative;
            color: black;
            padding: 0 30px;
            transition: 0.3s;
            overflow: hidden;
          }
          
          @media (max-width: 767px) {
             .main-nav > li > a {
              padding: 10px;
              font-size: 14px;
              height: 40px;
            }
          }
          
           .main-nav > li > a::before {
            content: "";
            width: 100%;
            height: 4px;
            position: absolute;
            top: 0;
            background-color: #ffb606;
            transition: 0.3s;
            left: -100%;
          }
          
           .main-nav > li > a::after {
            content: "";
            width: 100%;
            height: 100%;
            position: absolute;
            top: 0;
            background-color: #ffb606;
            transition: 0.3s;
            left: -100%;
          }
          
           .main-nav > li > a:hover {
            color: #ffb606;
            background-color: #fafafa;
          }
           .main-nav > li > a:hover::before {
            left: 0;
          }
          
        `}
      </style>
    </>
  );
};

export default Navbar;

import React, { FunctionComponent } from "react";
import Dish from "../assets/best-dish1.jpg";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";

interface Props {
  image: string;
  index: number;
}

const SpecialDish: FunctionComponent<Props> = ({ image, index }) => {
  return (
    <div
      className={`flex flex-col ${
        index % 2 === 0 ? "md:flex-row" : "md:flex-row-reverse"
      } justify-evenly items-center font-playfair mb-[40px] `}
    >
      <div className="w-full pl-[30px]  sm:pl-[0px] sm:w-3/4 md:w-2/4 mb-[20px]">
        <h1 className="block text-[35px] text-[#FFB606]">
          Garlic <span className="block text-[#131230]">green beans</span>
        </h1>
        <p className="text-[#777]">
          Be. Seed saying our signs beginning face give spirit own beast
          darkness morning moveth green multiply she'd kind saying one shall,
          two which darkness have day image god their night. his subdue so you
          rule can.
        </p>
        <p className="text-[25px] font-bold text-[#ffb606]">$12.00</p>
        <a href="" className="group">
          Book a table
          <FontAwesomeIcon
            icon={faArrowRight}
            className="pl-[10px] group-hover:animate-moveIcon group-hover:text-[#ffb606]"
          />
        </a>
      </div>
      <div className="w-full sm:w-3/4 md:w-1/4 ">
        <img
          className="w-full max-w-100% hover:opacity-[0.6] duration-700  rounded-[10px]"
          src={image}
          alt=""
        />
      </div>
    </div>
  );
};

export default SpecialDish;

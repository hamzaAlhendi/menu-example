import React from "react";
import logo from "../assets/logo.png";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faHeart,
  faLocationDot,
  faMapLocation,
  faMapMarked,
  faPhone,
} from "@fortawesome/free-solid-svg-icons";

const Footer = () => {
  const date = new Date().getFullYear();
  return (
    <>
      <div className="bg-[#131230]  w-full flex  font-playfair">
        <div className="grid grid-cols-1 p-[20px] md:p-0 md:grid-cols-2 lg:grid-cols-3 gap-9 mx-auto my-[80px] md:w-[750px] lg:w-[970px] xl:w-[1170px]">
          <div className=" text-[#777]">
            <div className=" flex flex-row items-center gap-3">
              <img src={logo} className="w-[50px] rounded-[50%]" alt="" />{" "}
              <span className=" text-white text-[20px] font-bold ">
                food fun
              </span>
            </div>
            <p className="mt-[20px]">
              Which morning fourth great won't is to fly bearing man. Called
              unto shall seed, deep, herb set seed land divide after over first
              creeping. First creature set upon stars deep male gathered said
              she'd an image spirit our
            </p>
          </div>
          <div className="text-[#777]">
            <h1 className="text-[#FFB606] font-bold text-[30px] mb-3">
              Contact Us
            </h1>
            <div className=" flex flex-row items-center gap-5 mb-4 text-[#777]">
              <FontAwesomeIcon icon={faLocationDot} className="pl-[10px]" />
              <p>1234 Some St San Francisco, CA 94102, US 1.800.123.4567</p>
            </div>
            <div className=" flex flex-row items-center gap-5 mb-4 text-[#777]">
              <FontAwesomeIcon icon={faPhone} className="pl-[10px]" />
              <p>(123) 456 78 90</p>
            </div>
            <div className=" flex flex-row items-center gap-5 mb-4 text-[#777]">
              <FontAwesomeIcon icon={faLocationDot} className="pl-[10px]" />
              <p>1234 Some St San Francisco, CA 94102, US 1.800.123.4567</p>
            </div>
            <div></div>
            <div></div>
          </div>
          <div className=" text-[#777]">
            <h1 className="text-[#FFB606] font-bold text-[30px] mb-3">
              Opening hours
            </h1>
            <p className="mb-3">Monday ...................... Closed</p>

            <p className="mb-3">Tue-Fri .............. 10 am - 12 pm</p>

            <p className="mb-3">Sat-Sun ............... 8 am - 11 pm</p>

            <p>Holidays ............. 10 am - 12 pm</p>
          </div>
        </div>
      </div>
      <div className="bg-[#0D0D27] w-full text-white flex text-center">
        <span className="w-full p-3">
          Copyright ©{date} All rights reserved | This template is made with
          <FontAwesomeIcon icon={faHeart} className="px-2" />
          by Hamza Alhendi
        </span>
      </div>
    </>
  );
};

export default Footer;

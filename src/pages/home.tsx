import React from "react";
import background from "../assets/background.jpg";
import foodSectionImage from "../assets/oie_mDkCepk4PlUP.png";
import reservationBackground from "../assets/reservation.jpg";
import Item from "../components/item";
import image1 from "../assets/item1.jpg";
import image2 from "../assets/item2.jpg";
import image3 from "../assets/item3.jpg";
import image4 from "../assets/item4.jpg";
import image5 from "../assets/item5.webp";
import image6 from "../assets/item6.jpg";
import specialDishImage1 from "../assets/best-dish1.jpg";
import specialDishImage2 from "../assets/best-dish1.jpg";
import reviewsBackground from "../assets/reviews.jpg";
import SpecialDish from "../components/special-dish";
// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";

// import Carousel from "react-bootstrap/Carousel";
// import Slider from "react-slick";
import Footer from "../components/footer";

function SampleNextArrow(props: any) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{
        ...style,
        display: "block",
      }}
      onClick={onClick}
    />
  );
}

function SamplePrevArrow(props: any) {
  const { className, style, onClick } = props;
  return (
    <div
      className={className}
      style={{ ...style, display: "block" }}
      onClick={onClick}
    />
  );
}

const Home = () => {
  var settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };
  return (
    <>
      <div
        className="background-image"
        style={{ backgroundImage: `url(${background})` }}
      >
        <div className="text">
          <p>THE MOST INTERESTING FOOD IN THE WORLD</p>
          <h1 className="font-serif">
            Discover the <span>flavors</span> of <span>foodfun</span>
          </h1>
        </div>
      </div>
      <div className="relative w-full overflow-hidden">
        <div className="food z-1 ">
          <div className=" z-[-10] absolute right-[-150px] top-[50%]">
            <img src={foodSectionImage} alt="" />
          </div>
          <div className="title">
            <h1>
              <span>we serve</span>

              <span>delicious food</span>
            </h1>
            <p>
              They're fill divide i their yielding our after have him fish on
              there for greater man moveth, moved Won't together isn't for fly
              divide mids fish firmament on net.
            </p>
          </div>
          <div className="container">
            <Item image={image1} key={1} />
            <Item image={image2} key={2} />
            <Item image={image3} key={3} />
            <Item image={image4} key={4} />
            <Item image={image5} key={5} />
            <Item image={image6} key={6} />
          </div>
        </div>
      </div>
      <div
        className="reservation"
        style={{ backgroundImage: `url(${reservationBackground})` }}
      >
        <div className="reservation-description">
          <h1>Natural ingredients and testy food</h1>
          <p>Some Trendy And Popular Courses Offerd</p>

          <button className="mt-5">RESERVATION</button>
        </div>
      </div>

      <div className="special-dishes">
        <div className="title">
          <h1>
            Our <span>special</span> dishes
          </h1>
          <p>Beast kind form divide night above let moveth bearing darkness.</p>
        </div>
        <div className="container">
          <SpecialDish image={specialDishImage1} index={1} />
          <SpecialDish image={specialDishImage2} index={2} />
        </div>
      </div>

      <div
        className="reviews"
        style={{ backgroundImage: `url(${reviewsBackground})` }}
      >
        <div className="overlay"></div>
        <div className="container flex flex-col items-center justify-center">
          <div className="mt-[50px]">
            <h1>
              Customer <span>says</span>
            </h1>
            <p>
              Beast kind form divide night above let moveth bearing darkness.
            </p>
          </div>
          {/* <div className="w-[50%] m-auto slider-container">
            <Slider {...settings}>
              <div className=" w-fit">
                <img className="w-[400px]" src={image1} alt="" />
              </div>
              <div className=" w-fit">
                <img className="w-[400px] " src={image2} alt="" />
              </div>
            </Slider>
          </div> */}
        </div>
      </div>

      <Footer />
      <style>{`
        .background-image{
            height:100vh;
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            position:relative;
            
        }
        .text{
            position:absolute;
            top:50%;
            left:50%;
            transform: translate(-50%,-50%);
        }
        .text p{
            color:white;
            font-size:25px;
            text-align:center;
        }
        .text h1{
            color:white;
            font-size:80px;
            font-family: ui-serif, Georgia, Cambria, "Times New Roman", Times, serif;
            font-style:italic;
            text-align:center;
            
        }

        @media (max-width:992px){
            .text h1{
                font-size:50px;
            }
        }
        .text h1 span{
            color:#FFB606;
            
        }

        /* food section start */

        .food{
            padding:80px 0px;
            margin:auto;
        }

        @media (min-width: 768px) {
            .food {
              width: 750px;
            }
          }
        @media (min-width: 992px) {
          .food {
            width: 970px;
          }
        }
        @media (min-width: 1200px) {
          .food {
            width: 1170px;
          }
        }
        .food .title{
            width:500px;
            margin-bottom:80px;
        }
        @media (max-width: 992px) {
            .food .title{
                width:100%;
                text-align:center;
                padding:0px 20px;
            }
        }
        .food .title h1 span{
            display:block;

        }
        .food .title h1 span:first-child{
            color:#FFB606;
            font-size:31px;
            font-weight:bold;
        }
        .food .title h1 span:nth-child(2){
            color:#131230;
            font-size:31px;
            font-weight:bold;

        }
        .food .title p{
            color:#777;
            font-size:20px;

        }
        .food .container{
            display:grid;
            grid-template-columns: repeat(auto-fill, minmax(300px, 1fr));
            gap: 35px;
            margin:auto;
        }

        /* food section end */

        /* reservation section start */

        .reservation{
            width:100%;
            height:500px;
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            position:relative;
        }

        
        
        .reservation .reservation-description{
            position:absolute;
            top:50%;
            left:50%;
            transform: translate(-50%,-50%);
            color:white;
            text-align:center;
            font-family:"Playfair Display",serif;
            font-size:25px;
        }

        @media (max-width: 768px) {
            .reservation .reservation-description{
                width:100%;
            }
        }


        .reservation .reservation-description h1{
            font-size:40px;
            font-family:"Playfair Display",serif;
            font-weight:600;

        }

       
        .reservation .reservation-description p{
            font-style:italic;
            margin:30px 0px;
        }

        .reservation .reservation-description button {
            margin-top:20px;
            border:solid 1px #FFB606;
            background-color:#FFB606;
            color:#131230;
            padding:10px 20px;
            transition:0.6s;
        }
        .reservation .reservation-description button:hover {
            border:solid 1px #FFB606;
            background-color:transparent;
            color:#FFB606;
        }

        /* reservation section end */

        /* special-dishes section start */


        .special-dishes{
            padding:80px 0px;
            margin:auto;
            margin-top:40px;
            padding:0px 20px;
        }

        .special-dishes .container{
            margin:auto;
        }

        @media (min-width: 768px) {
            .special-dishes {
              width: 750px;
            }
          }
        @media (min-width: 992px) {
          .special-dishes {
            width: 970px;
          }
        }
        @media (min-width: 1200px) {
          .special-dishes {
            width: 1170px;
          }
        }

        .special-dishes .title{
            text-align:center;
            font-family: "Playfair Display",serif;
            margin-bottom:40px;

        }

        .special-dishes .title h1{
            font-weight:bold;
            font-size:40px;
            font-family: "Playfair Display",serif;
            color:#131230;
            
        }

        @media (max-width:768px){
        .special-dishes .title h1{
            font-size:30px;
        }

        }
        .special-dishes .title h1 span{
            color:#FFB606;
        }

        .special-dishes .title p{
            color:#777;
            font-style:italic;
        }

        /* special-dishes section end */


        /* reviews section end */

        .reviews {
            background-color:#4D5157;
            width:100%;
            background-size: cover;
            background-position: center;
            background-repeat: no-repeat;
            font-family: "Playfair Display",serif;
            position:relative;
            z-index:0;

        }
         
        

        .reviews .container{
            margin:auto;
            text-align:center;
            
        }
        .reviews .container h1{
            display:block;
            margin-top:20px;
            font-size:35px;
            font-weight:600;
            color:white;
        }

        .reviews .container h1 span{
            color:#FFB606;
        }

        .reviews .container p{
            color:white;
            font-style:italic;
            margin-bottom:30px;
        }
        /* reviews section end */

       



        `}</style>
    </>
  );
};

export default Home;

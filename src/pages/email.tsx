import React from "react";
import background from "../assets/blank.png";
import containerBackground from "../assets/container-background.png";

const Email = () => {
  return (
    <>
      <div
        className="background "
        style={{ backgroundImage: `url(${background})` }}
      ></div>
      <div className="book-table">
        <div
          className="container"
          style={{ backgroundImage: `url(${containerBackground})` }}
        >
          <h1>
            Book <span>your</span> table
          </h1>
          <p>Beast kind form divide night above let moveth bearing darkness.</p>

          <div className="form">
            <form action="" className="w-[60%]">
              <input
                className="input rounded-[10px]"
                type="text"
                placeholder="Your Name"
                name="name"
              />
              <input
                className="input rounded-[10px]"
                type="email"
                placeholder="Your Email"
                name="email"
              />
              <input
                className="input rounded-[10px]"
                type="text"
                placeholder="Your Phone"
                name="mobile"
              />
              <div className="input-container">
                <input type="date" id="date" name="date" />
                <input type="time" id="time" name="time" />
              </div>

              <textarea
                className="input rounded-[10px]"
                placeholder="Tell Us About Your Needs"
                name="message"
              ></textarea>
              <button
                className="bg-[#FFB606] text-white px-[20px] text-[20px] py-[10px] mb-3 hover:bg-transparent hover:text-[#FFB606] transition font-bold duration-400"
                type="button"
                value="Send"
              >
                Send
              </button>
            </form>
          </div>
        </div>
      </div>
      <style>
        {`
            /* book-table section end */
            
            .background{
                height:100vh;
                background-size: cover;
                background-position: center;
                background-repeat: no-repeat;
                position:relative;
                
            }

            .book-table{
                background-color:#121212;
                position:absolute;
                top:50%;
                left:50%;
                transform:translate(-50%,-50%);
                display:flex;
                border-radius:10px;
                overflow:hidden;
                width:80%;
            }
    
            .book-table .container{
                margin:auto;
                text-align:center;
                background-size: cover;
                background-position: center;
                background-repeat: no-repeat;
                position:relative;
                width:100%;
            
            }

            @media (min-width: 768px) {
                .book-table {
                  width: 750px;
                }
              }
            @media (min-width: 992px) {
              .book-table {
                width: 970px;
              }
            }
            @media (min-width: 1200px) {
              .book-table {
                width: 1000px;
              }
            }
    
            .book-table .container h1{
                display:block;
                margin-top:20px;
                font-size:35px;
                font-weight:600;
                color:white;
            }
    
            .book-table .container h1 span{
                color:#FFB606;
            }
    
            .book-table .container p{
                color:white;
                font-style:italic;
                margin-bottom:30px;
            }
            
            .book-table .form {
                display: flex;
                justify-content: center;
                align-items: center;
                
              }
              
            
              .book-table .form .input {
                display: block;
                width: 100%;
                margin-bottom: 25px;
                padding: 15px;
                border: none;
                border-bottom: solid 1px #ccc;
                background-color: #f9f9f9;
              }
              
              .book-table .form textarea.input {
                resize: none;
                height: 200px;
              }
              
              .book-table .form .input:focus {
                outline: none;
                caret-color: #FFB606;
              }
              
              .book-table .form input[type="button"] {
                display: block;
                width: 100%;
                padding: 15px;
                background-color: var(--main-color);
                color: white;
                font-weight: bold;
                border: none;
                cursor: pointer;
                transition: var(--main-transition-duration);
              }
              
              .book-table .form input[type="button"]:hover {
                background-color: var(--main-color-alt);
              }

              .input-container {
                display: flex;
                flex-direction: row;
                justify-content:space-around;
                
                margin: auto;
                padding: 20px;
              }
              
              @media (max-width: 768px) {
                .input-container{
                    flex-direction: column;
                 
                }
              }
              
              
              .input-container input {
                margin-bottom: 20px;
                padding: 10px;
                border: 1px solid #ccc;
                border-radius: 4px;
              }
            /* book-table section end */
            `}
      </style>
    </>
  );
};

export default Email;

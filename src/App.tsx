import { createBrowserRouter } from "react-router-dom";
import { Root } from "./routes/root";
import Home from "./pages/home";
import Email from "./pages/email";

export const router = createBrowserRouter([
  {
    path: "/",
    element: <Root />,
    children: [
      {
        path: "/",
        element: <Home />,
      },
      {
        path: "/email",
        element: <Email />,
      },
    ],
  },
]);
